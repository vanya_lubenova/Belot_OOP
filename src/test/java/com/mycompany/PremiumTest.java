package com.mycompany.game;

import com.mycompany.enums.Suit;
import com.mycompany.enums.Value;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by asus on 5/15/17.
 */
public class PremiumTest {
    Card card = new Card(Value.JACK, Suit.SPADE);
    Card other = new Card(Value.SEVEN, Suit.CLUB);

    @Test
    public void sortBySuit(){
        Premium premium = new Premium();
        premium.addCard(card);
        premium.addCard(other);
        premium.sortBySuit();
        assertTrue(other.equals(premium.getCard(0)));
    }
    @Test
    public void sortByValue(){
        Premium premium = new Premium();
        premium.addCard(card);
        premium.addCard(other);
        premium.sortByValue();
        assertTrue(other.equals(premium.getCard(0)));
    }

}