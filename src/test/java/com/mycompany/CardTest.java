package com.mycompany.game;

import com.mycompany.enums.Value;
import org.junit.Test;

import static com.mycompany.enums.Suit.SPADE;
import static org.junit.Assert.*;

/**
 * Created by asus on 5/12/17.
 */
public class CardTest {
    Card card = new Card(Value.JACK, SPADE);
    @Test
    public void getSuit(){
        assertEquals(SPADE, card.getSuit());
    }
    @Test
    public void getValue(){
        assertEquals(Value.JACK, card.getValue());
    }
    @Test
    public void equals(){
        Card other = new Card(Value.JACK, SPADE);
        assertTrue(card.equals(other));
    }

}