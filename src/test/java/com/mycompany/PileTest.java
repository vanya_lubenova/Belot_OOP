package com.mycompany.game;

import com.mycompany.enums.Suit;
import com.mycompany.enums.Value;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by asus on 5/12/17.
 */
public class PileTest {
    Card card = new Card(Value.JACK, Suit.SPADE);
    Card other = new Card(Value.SEVEN, Suit.CLUB);

    @Test
    public void addCard(){
        Pile pile = new Pile();
        pile.addCard(card);
        assertTrue(pile.cards.size() == 1);
    }
    @Test
    public void getCard(){
        Pile pile = new Pile();
        pile.addCard(card);
        assertTrue(pile.getCard(0).equals(card));
    }
    @Test
    public void removeCard(){
        Pile pile = new Pile();
        pile.addCard(card);
        pile.removeCard(card);
        assertTrue(pile.cards.size() == 0);
    }

}