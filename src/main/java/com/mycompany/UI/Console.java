package com.mycompany.UI;

/**
 * Created by asus on 5/15/17.
 */
public class Console {
    static final String start = "New Game" + "\nPress ENTER to continue";
    static final String split = "Splits player %d";
    static final String distribution = "Distributes player %d";
    static final String annonce = "Annonce player %d";
    static final String premium = "Premium player %d";
    static final String result = "RESULT\n" + "Our     Their\n" + "%d     %d";
    static final String win = "You win!";
    static final String lose = "You lose!";

    public static void print(String msg) {
        System.out.println(msg);
    }
    public static void printStart() {
        print(start);
    }
    public static String printSplit(int player) {
        return String.format(split, player);
    }
    public static String printDistribution(int player) {
        return String.format(distribution, player);
    }
    public static String printAnnonce(int player) {
        return String.format(annonce, player);
    }
    public static String printPremium(int player) {
        return String.format(premium, player);
    }
    public static String printResult(int ourPoints, int theirPoints) {
        return String.format(result, ourPoints, theirPoints);
    }
    public static void printWin() {
        print(win);
    }
    public static void printLose() {
        print(lose);
    }
}
