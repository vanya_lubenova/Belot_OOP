package com.mycompany.game;

import com.mycompany.enums.Suit;
import com.mycompany.enums.Value;

/**
 * Created by asus on 5/12/17.
 */
public class Card {
    private Value value = null;
    private Suit suit = null;
    public Card(Value value, Suit suit)
    {
        this.value = value;
        this.suit = suit;
    }
    public Value getValue(){return this.value;}
    public Suit getSuit(){return this.suit;}
    public boolean equals(Card card){
        boolean flag = false;
        if(this.value == card.value && this.suit == card.suit) {
            flag = true;
        }
        return flag;
    }
}
