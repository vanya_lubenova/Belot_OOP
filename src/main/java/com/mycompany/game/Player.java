package com.mycompany.game;

import java.util.Stack;

/**
 * Created by asus on 5/12/17.
 */
abstract class Player extends Pile {
    final int numberOfPlayers = 4;
    private final int number;
    Player(int number)
    {
        this.number = number;
    }
    public int getPlayerNumber(){return number;}

    public void distribute(Player next, Player next2, Player next3, CardDeck deck)
    {
        for (int i = 0; i < numberOfPlayers; i++) {

            addToHandThree(i, this.cards, deck);
            addToHandThree(i, next.cards, deck);
            addToHandThree(i, next2.cards, deck);
            addToHandThree(i, next3.cards, deck);
        }
        for (int i = 0; i < numberOfPlayers; i++) {

            addToHandTwo(i, this.cards, deck);
            addToHandTwo(i, next.cards, deck);
            addToHandTwo(i, next2.cards, deck);
            addToHandTwo(i, next3.cards, deck);
        }
    }
    public void addToHandThree(int i, Stack<Card> hand, CardDeck deck) {
        hand.add(cards.get(i));
        hand.add(cards.get(i + 1));
        hand.add(cards.get(i + 2));
    }
    public void addToHandTwo(int i, Stack<Card> hand, CardDeck deck) {
        hand.add(cards.get(i));
        hand.add(cards.get(i + 1));
    }

    abstract public String bidding(String previousBid);

}
