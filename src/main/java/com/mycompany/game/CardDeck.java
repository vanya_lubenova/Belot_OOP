package com.mycompany.game;

import com.mycompany.enums.Suit;
import com.mycompany.enums.Value;

import java.util.Collections;
import java.util.Stack;

/**
 * Created by asus on 5/12/17.
 */
public class CardDeck {
    private final int numberOfCards = 32;

    Stack<Card> cards = new Stack<Card>();
    public void generateCardDeck() {
        int countCards = 0;
        while(countCards < numberOfCards) {
            for (Value value : Value.values()) {
                for (Suit suit : Suit.values()) {
                    cards.push(new Card(value, suit));
                    countCards++;
                }
            }
        }
    }

    public void shuffle() {
        Collections.shuffle(this.cards);
    }
    public void split(){
        cards.spliterator();
    }
}
