package com.mycompany.game;

import com.mycompany.enums.Suit;
import com.mycompany.enums.Value;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by asus on 5/12/17.
 */
public class Game {

    private int ourPoints = 0;
    private int theirPoints = 0;
    private static Map<Value, Integer> trumpPoints = new HashMap<Value, Integer>();
    private static Map<Value, Integer> noTrumpPoints = new HashMap<Value, Integer>();
    Game()
    {
        trumpPoints.put(Value.SEVEN, 0);
        trumpPoints.put(Value.EIGHT, 0);
        trumpPoints.put(Value.NINE, 14);
        trumpPoints.put(Value.TEN, 10);
        trumpPoints.put(Value.JACK, 20);
        trumpPoints.put(Value.QUEEN, 3);
        trumpPoints.put(Value.KING, 4);
        trumpPoints.put(Value.ACE, 11);

        noTrumpPoints.put(Value.SEVEN, 0);
        noTrumpPoints.put(Value.EIGHT, 0);
        noTrumpPoints.put(Value.NINE, 0);
        noTrumpPoints.put(Value.TEN, 10);
        noTrumpPoints.put(Value.JACK, 2);
        noTrumpPoints.put(Value.QUEEN, 3);
        noTrumpPoints.put(Value.KING, 4);
        noTrumpPoints.put(Value.ACE, 11);

    }
    public Integer calculateTrumpPoints(Value value)
    {
        return trumpPoints.get(value);
    }
    public Integer calculateNoTrumpPoints(Value value)
    {
        return noTrumpPoints.get(value);
    }

    public Card trumpCompare(Pile pile, Suit suit) {
        return null;
    }
    public Card noTrumpCompare(Pile pile) {
        return null;
    }
    public Card allTrumpCompare(Pile pile) {
        return null;
    }

    public void play(Player user,Player bot1, Player bot2, Player bot3)
    {

    }


}
