package com.mycompany.game;

import java.util.Stack;

/**
 * Created by asus on 5/12/17.
 */
public class Pile {
    public Stack<Card> cards = new Stack<Card>();
    public Card getCard(int index) {
        return this.cards.get(index);
    }
    public void addCard(Card c) {
        this.cards.push(c);
    }
    public void removeCard(Card c) {
        for (int i = 0; i < cards.size(); i++) {
            if (cards.get(i).equals(c)){
                this.cards.remove(i);
            }

        }
    }
}
