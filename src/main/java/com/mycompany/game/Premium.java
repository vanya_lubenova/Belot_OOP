package com.mycompany.game;

import com.mycompany.enums.Suit;
import com.mycompany.enums.Value;

import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

/**
 * Created by asus on 5/15/17.
 */
public class Premium extends Pile{

    private static Map<String, Integer> premiums = new HashMap<String, Integer>();
    Premium(){};
    Premium(Pile hand)
    {
        cards = hand.cards;
        premiums.put("tierce", 20);
        premiums.put("quarte", 50);
        premiums.put("quint", 100);
        premiums.put("carre-Jacks", 200);
        premiums.put("carre-Nines", 150);
        premiums.put("carre-Aces, Kings, Queens, or Tens", 100);
    }
    public Integer getPremiumPoints(String premium)
    {
        return premiums.get(premium);
    }
    public void sortBySuit()
    {
        Stack<Card> tmp = new Stack<Card>();
        int cardCount = 0;
        int suitCount = 0;
        while(cardCount < cards.size()) {
            for (int i = 0; i < cards.size(); i++) {
                if (cards.get(i).getSuit() == Suit.values()[suitCount]) {
                    tmp.push(cards.get(i));
                    cardCount++;
                }
            }
            suitCount ++;
        }
        cards = tmp;
    }
    public void sortByValue()
    {
        Stack<Card> tmp = new Stack<Card>();
        int cardCount = 0;
        int suitCount = 0;
        while(cardCount < cards.size()) {
            for (int i = 0; i < cards.size(); i++) {
                if (cards.get(i).getSuit() == Suit.values()[suitCount]) {
                    tmp.push(cards.get(i));
                    cardCount++;
                }
            }
            suitCount ++;
        }
        cards = tmp;
    }
    public int countSuit(Suit suit)
    {
        int countSuit = 0;
        for (int i = 0; i < cards.size(); i++) {
            if (cards.get(i).getSuit() == suit)
            {
                countSuit++;
            }

        }
        return countSuit;
    }
    public int countValue(Value value)
    {
        int countValue = 0;
        for (int i = 0; i < cards.size(); i++) {
            if (cards.get(i).getValue() == value) {
                countValue++;
            }
        }
        return countValue;
    }
}
