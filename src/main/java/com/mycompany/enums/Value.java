package com.mycompany.enums;

/**
 * Created by asus on 5/12/17.
 */
public enum Value {
    SEVEN (0, 0),
    EIGHT   (1, 1),
    NINE   (2, 6),
    JACK (3, 7),
    QUEEN  (4, 2),
    KING  (5, 3),
    TEN  (6, 4),
    ACE (7, 5);
    private final int trumpWeight;
    private final int noTrumpWeight;
    Value(int trumpValue, int noTrumpValue) {
        this.trumpWeight = trumpValue;
        this.noTrumpWeight = noTrumpValue;
    }
    public int getTrumpWeight(){ return this.trumpWeight;}
    public int getNoTrumpWeight(){return this.noTrumpWeight;}
}
