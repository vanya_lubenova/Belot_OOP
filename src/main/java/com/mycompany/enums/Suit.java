package com.mycompany.enums;

/**
 * Created by asus on 5/12/17.
 */
public enum Suit {
    CLUB,
    DIAMOND,
    HEART,
    SPADE,
}
